<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxItem extends Model
{

    public function box(){
        return $this->belongsTo(Box::class, 'box_id');
    }
    //
}
