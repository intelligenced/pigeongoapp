<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Box;
use App\BoxItem;
use App\BoxClaimer;

use Auth;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function step1(){

        return view('auth.boxes.step1');

    }

    
    public function storestep1(Request $request){

        $box = new Box;
        $box->name = $request->name;
        $box->created_by = Auth::user()->id;
        $box->save();

        return redirect()->action('BoxController@step2',['box_id' => $box->id]);

    }


    public function step2($box_id){
        $box = Box::find($box_id);
        return view('auth.boxes.step2',['box' => $box]);

    }


    public function storeItem(Request $request){

        $box = Box::find($request->box_id);
        if($box){
            $boxItem = new BoxItem;
            $boxItem->box()->associate($box);
            $boxItem->name= $request->name;
            $boxItem->save();
        }

        return redirect()->back();
    }

    public function removeItem(Request $request){
        $boxItem = BoxItem::find($request->box_item_id);
        $boxItem->delete();

        return redirect()->back();
    }


    public function step3($box_id){

        $box= Box::find($box_id);

        return view('auth.boxes.step3',['box' => $box]);

    }


    public function storePickup(Request $request){
        $box = Box::find($request->box_id);
        $box->pickup_lat = $request->pickup_lat;
        $box->pickup_long = $request->pickup_long;
        $box->pickup_address = $request->pickup_address;
        $box->update();
     
        return redirect()->back();
        
    }

    public function step4($box_id){
        return view('auth.boxes.step4',['box' => Box::find($box_id)]);
    }

    public function storeDropoff(Request $request){
        $box = Box::find($request->box_id);

    
        
        $box->drop_lat = $request->drop_lat;
        $box->drop_long = $request->drop_long;
        $box->drop_address = $request->drop_address;
        $box->update();
        return redirect()->action('BoxController@step5',['box' => $box]);
        
    }

    public function step5($box_id){
        return view('auth.boxes.step5',['box' => Box::find($box_id)]);
    }

    public function summary($box_id){
        return view('auth.boxes.summary',['box' => Box::find($box_id)]);
    }



    public function myboxes(){
        return view('auth.boxes.my',['boxes' => Box::where('created_by',Auth::user()->id)->get()]);
    }

    public function browse(){
        return view('auth.boxes.browse',['boxes' => Box::where('is_claimed_by',NULL)->where('created_by','!=',Auth::user()->id)->get()]);
    }

    public function browseMap(){
        return view('auth.boxes.browseMap',['boxes' => Box::where('is_claimed_by',NULL)->select('id', 'pickup_lat as lat', 'pickup_long as long', 'pickup_address as add')->get()]);
    }

    public function claim($box_id){

        $box= Box::find($box_id);

        if(BoxClaimer::where('box_id', $box->id)->where('claimed_by',Auth::user()->id)->first()== null){
            $box_claimer = new BoxClaimer;
            $box_claimer->box()->associate($box);
            $box_claimer->claimed_by = Auth::user()->id;
            $box_claimer->save();
        }


        return redirect()->back();
    }

    public function approve($box_id,$user_id){


        $box= Box::find($box_id);
        $claimed_boxes = $box->box_claimers()->get();
        foreach($claimed_boxes as $claimed_box){
            $claimed_box->delete();
        }

        $box->is_claimed_by = $user_id;
        $box->update();

        

        return redirect()->back();
    }

    public function mydeliveries(){

        $claimed_boxes = BoxClaimer::where('claimed_by', Auth::user()->id)->get();
        $boxes = Box::where('is_claimed_by',Auth::user()->id)->get();

        return view('auth.boxes.mydelivery',['claimed_boxes'=> $claimed_boxes, 'boxes' => $boxes]);
    } 

    public function delivered($box_id){
        $box = Box::find($box_id);
        $box->is_delivered = 1;
        $box->update();

        return redirect()->back();
    }


    public function completed($box_id){
        $box = Box::find($box_id);
        $box->is_completed = 1;
        $box->update();

        return redirect()->back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Ht
     * tp\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
