<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoxClaimer extends Model
{

    public function box(){
        return $this->belongsTo(Box::class, 'box_id');
    }

    public function claimed_by(){
        return $this->belongsTo(User::class, 'claimed_by');
    }
    //
}
