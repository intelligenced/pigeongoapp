<?php

namespace App;

use App\BoxClaimer;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Box extends Model
{

    protected $appends = ['claimed'];

    public function box_items(){
        return $this->hasMany(BoxItem::class, 'box_id');
    }

    public function box_claimers(){
        return $this->hasMany(BoxClaimer::class, 'box_id');
    }

    public function created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function is_claimed_by(){
        return $this->belongsTo(User::class, 'is_claimed_by');
    }

    public function getClaimedAttribute(){
        $box_claimer = BoxClaimer::where('box_id', $this->id)->where('claimed_by',Auth::user()->id)->first();
        if($box_claimer){
            return true;
        }else{
            return false;
        }

    }
    //
}
