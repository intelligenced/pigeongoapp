@extends('layouts.frontpage')
@section('content')
 
      
        
<div class="is-size-4 " style="text-align:center">
    Add a Box
</div>
<br>


<div class="is-size-3 " style="text-align:center">
  <img src="{{ URL::to('/') }}/img/box-outline-filled.png" alt="Smiley face" height="100" width="100">
</div>
<br>

{!! Form::open(['action' => 'BoxController@storestep1', 'method' => 'post'])!!}
<div class="is-size-5 " style="text-align:center">
    <p>Give your box a name,</p> <span>something to identify it by</span>
</div>

<div class="field" style="text-align:center">
    <label class="label"></label>
    <div class="control">
      <input class="input is-large" type="text" name="name" placeholder="">
    </div>
  </div>

  <div class="field is-grouped is-grouped-right" style="text-align:center">
    <p class="control">

      <input class="button is-primary" type="submit" value="next"/>

    </p>
    <p class="control">
      <a class="button is-light">
        Cancel
      </a>
    </p>
  </div>
  

  {!! Form::close() !!}
  
<div class="columns">
<div class="column is-one-fifth">



</div>
   






</div>


  @endsection