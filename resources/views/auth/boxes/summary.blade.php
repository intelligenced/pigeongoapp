@extends('layouts.frontpage')
@section('content')
 
      
        
<div class="is-size-4 ">
    Summary
</div>
<br>



  <b>Box Name: </b><p>{{$box->name}}</p>
 


  <b>Items: </b>
  <div class="control">
  <ul>
  
  @foreach($box->box_items()->get() as $boxitem)<br>
    <li>
  <p>{{$boxitem->name}}</p>
  </li>
  @endforeach
  </ul>
  </div>


  <br><b>Pick Up Point: </b> <br> <p>{{$box->pickup_address}}</p>
  


  <b>Drop Off Point: </b> <br> <p>{{$box->drop_address}}</p>

<br/>


<b> Claim Status </b> <br>
@if($box->is_claimed_by == null)
<p> Unclaimed </b>
@else
<p> Claimed </p>
@endif



 <br><b> Delivery Status </b> <br>
@if($box->is_delivered == 1)
<p> Delivered </b> <br>
@else
<p> Awaiting Delivery </p> <br>
@endif




<b> Completion Status </b> <br>
@if($box->is_completed == 1)
<p> Completed </b> <br>
@else
<p> Pending </p> <br>
@endif

<div id="mapid" style="height:400px;">
    </div>





<script>
$( document ).ready(function() {

var mymap = L.map('mapid').setView(
  [4.175415, 73.510201], 18);
  mapLink = 
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(mymap);

var pickpopup = new L.popup();
var droppopup = new L.popup();



var plat = '<?php echo $box->pickup_lat;?>';
var plng = '<?php echo $box->pickup_long;?>';

var dlat = '<?php echo $box->drop_lat;?>';
var dlng = '<?php echo $box->drop_long;?>';

var picklatlng = new L.latLng(plat, plng);;
var droplatlng = new L.latLng(dlat, dlng);;


var x = new L.marker(picklatlng).addTo(mymap);
x.bindPopup("Pick-Up Area");
x.addTo(mymap);

var y = new L.marker(droplatlng).addTo(mymap);
y.bindPopup("Drop-Off Area");
y.addTo(mymap);


// navigator.geolocation.getCurrentPosition(function(location) {
//   var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
//   var marker = L.marker(latlng).addTo(mymap);
// });

// data = [picklatlng, droplatlng]
// function connectTheDots(data){
//     var c = [];
//     for(i in data._layers) {
//         var x = data._layers[i]._latlng.lat;
//         var y = data._layers[i]._latlng.lng;
//         c.push([x, y]);
//     }
//     return c;
// }

// pathCoords = connectTheDots(window.geojson);
// var pathLine = L.polyline(pathCoords).addTo(map)


});



</script>

  @endsection