@extends('layouts.frontpage')
@section('content')
 
      
        
<div class="is-size-4 ">
    Drop-Off Location
</div>
<br>


{!! Form::open(['action' => 'BoxController@storeDropoff', 'method' => 'post'])!!}
<input type="hidden" name="box_id" value="{{$box->id}}">

    <div id="mapid" style="height:400px;">
    </div>
    {{ Form::hidden('drop_lat', $box->drop_lat, array('id' => 'DropOffLocationLat')) }}
    {{ Form::hidden('drop_long',$box->drop_long, array('id' => 'DropOffLocationLng')) }}
    <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">Address: </label>

                            <div class="col-md-6">
        <input type="text" name="drop_address" value="{{$box->drop_address}}" class="input" required autofocus>
        <div class="field is-grouped is-grouped-right">
    <p class="control">
    <br/>
      <input class="button is-primary" type="submit" value="next"/>

    </p>
    <p class="control">
    <br/>
      <a class="button is-light">
        Cancel
      </a>
    </p>
  </div>
</div>



</div>

  {!! Form::close() !!}
  
<div class="columns">
<div class="column is-one-fifth">



</div>
   






</div>



<script>
$( document ).ready(function() {

var mymap = L.map('mapid').setView(
  [4.175415, 73.510201], 18);
  mapLink = 
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(mymap);

var popup = L.popup();

function onMapClick(e) {

$('#DropOffLocationLat').val(e.latlng.lat);
$('#DropOffLocationLng').val(e.latlng.lng);
    popup
        .setLatLng(e.latlng)
        .setContent("Drop-Off Area")
        .openOn(mymap);
}

mymap.on('click', onMapClick);




navigator.geolocation.getCurrentPosition(function(location) {
  var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
  var marker = L.marker(latlng).addTo(mymap);
});


});


</script>

  @endsection