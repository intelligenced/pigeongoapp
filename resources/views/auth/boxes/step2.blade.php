@extends('layouts.frontpage')
@section('content')
 
      
        
<div class="is-size-4 " style="text-align:center">
   Add Items to Box : {{$box->name}}
</div>
<br>


<div class="is-size-3 " style="text-align:center">
<img src="{{ URL::to('/') }}/img/box-outline-filled.png" alt="Smiley face" height="100" width="100">
</div>
<br>

{!! Form::open(['action' => 'BoxController@storeItem', 'method' => 'post'])!!}
<div class="is-size-5 " style="text-align:center">
    Add an item
</div>

<input type="hidden" name="box_id" value="{{$box->id}}" style="text-align:center" />

<div class="field" style="text-align:center">
    <label class="label"></label>
    <div class="control">
      <input class="input is-medium" type="text" name="name" placeholder="">
    </div>
  </div>

  <div class="field is-grouped is-grouped-right">
    <p class="control">

      <input class="button is-primary" type="submit" value="Add"/>

    </p>
    <p class="control">
      <a class="button is-light">
        Cancel
      </a>
    </p>
  </div>
  

  {!! Form::close() !!}


  <hr>

  @if($box->box_items()->get()->count() > 0)

  @foreach($box->box_items()->get() as $box_item)

<div class="field is-grouped">
    <p class="control is-expanded">

    {{$box_item->name}}
    </p>
    <p class="control">
      <a class="button is-danger">
        Delete
      </a>
    </p>
  </div>
  @endforeach

  @endif
  
  <a class="button is-primary" href="{{ action('BoxController@step3', $box->id)}}">
      Next
    </a>


<div class="columns">
<div class="column is-one-fifth">



</div>
   






</div>


  @endsection