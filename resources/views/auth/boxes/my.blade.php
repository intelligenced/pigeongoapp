@extends('layouts.frontpage')
@section('content')

<a class="button is-primary is-outlined is-fullwidth" href="{{action('BoxController@step1')}}">Add Package</a>
<hr>


<div class="is-size-2 ">
       My Packages
    </div>
    <br>
<div class="columns">
            
            @if($boxes!= null)

                @foreach($boxes as $box)
                <div class="column is-one-third">

                <div class="card">

                        <div class="card-content">
                          <div class="media">
                            <div class="media-left">

                            </div>
                            <div class="media-content">
                              <p class="title is-4"> {{$box->name}} > {{$box->pickup_address or '?'}} > {{$box->drop_address or '?'}}</p>
                              <p class="subtitle is-6">@ {{$box->created_by()->first()->name}}</p>
                            </div>
                          </div>
                      
                          <div class="content">

                                @if($box->box_items()->get()->count() > 0)
                                Items -
                                    @foreach($box->box_items()->get() as $box_item)

                                {{$box_item->name}} ,

                                    @endforeach

                                @endif

                                

                                @if($box->is_claimed_by!= null)
                                <br>

                                <hr>

                                <div class="media">
                                    <div class="media-left">
                                      <figure class="image is-48x48">
                                        <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                      </figure>
                                    </div>
                                    <div class="media-content">
                                      <p class="title is-4"> 

                                        @if($box->is_delivered == 0)
                                        
                                        Delivery in progress  

                                        @endif
                       
                                        @if($box->is_delivered == 1 && $box->is_completed == 0)
                                        
                                        <div class="field is-grouped is-grouped-right">
                                            <p class="control">
                                              <a class="button is-outlined is-primary" href="{{ action('BoxController@completed', $box->id) }}">
                                                  Complete
                                              </a>
                                            </p>
                                          </div>

                                        @endif   
                                        
                                        
                                        @if($box->is_completed == 1)

                                          Delivery Completed Successfully


                                        @endif
                                      
                                      
                                      </p>
                                      <p class="subtitle is-6">@  {{$box->is_claimed_by()->first()->name }}</p>
                                    </div>
                                  </div>




                                @else


                                @if($box->box_claimers()->get()->count() > 0)
                                @foreach($box->box_claimers()->get() as $box_claimer)

                                <hr>

                                 <br><b>Claimed By : </b><br>{{$box_claimer->claimed_by()->first()->name}} 
                                 <br><b>Rated : </b><br>4 Stars <br>


                                 <div class="field is-grouped is-grouped-right">
                                          <p class="control">
                                            <a class="button is-outlined is-primary" href="{{ action('BoxController@approve', [$box->id, $box_claimer->claimed_by]) }}">
                                                Approve
                                            </a>
                                          </p>
                                        </div>


                                @endforeach

                                @else

                                <div class="has-text-primary">Unclaimed </div>

                                @endif


                                @endif






                                <hr>
                                <a class="button is-medium is-fullwidth" href="{{action('BoxController@summary', $box->id )}}">View</a>


                          </div>
                        </div>
                      </div>

                    </div>

                @endforeach

            @endif



        

      </div>

@endsection