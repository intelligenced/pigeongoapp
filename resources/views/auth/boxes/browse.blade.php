@extends('layouts.frontpage')
@section('content')
<a class="button is-primary is-outlined is-fullwidth" href="{{action('BoxController@mydeliveries')}}">My Deliveries</a>

<a class="button is-primary is-outlined is-fullwidth" href="{{action('BoxController@browseMap')}}">Delivery Map</a>


<hr>

<div class="is-size-3 ">
       Delivery Packages near your area
    </div>
    <br>



  
<div class="columns">
            
            @if($boxes!= null)

                @foreach($boxes as $box)
                <div class="column is-one-third">

                <div class="card">

                        <div class="card-content">
                          <div class="media">
                            <div class="media-left">

                            </div>
                            <div class="media-content">
                              <p class="title is-4"> Box : {{$box->name}} | From : {{$box->pickup_address or '?'}} >  To :{{$box->drop_address or '?'}}</p>
                              <p class="subtitle is-6">@ {{$box->created_by()->first()->name}}</p>
                            </div>
                          </div>
                      
                          <div class="content">

                                @if($box->box_items()->get()->count() > 0)
                                <br><b>Items</b><br>
                                    @foreach($box->box_items()->get() as $box_item)

                                {{$box_item->name}} ,

                                    @endforeach

                                @endif

                                <br><b>Distance from your location</b><br> 12 km
                                <br><b>Estimated Price</b><br> 12 MVR

                                


                                <div class="has-text-primary">Unclaimed </div>


                                <div class="field is-grouped is-grouped-right">

                                  @if($box->claimed == false)
                                        <p class="control">
                                                <a class="button  is-primary is-outlined" href="{{action('BoxController@claim',$box->id)}}">
                                                        Claim
                                                      </a>
                                        </p>

                                        @endif
                                        <p class="control">
                                          <a class="button is-light" href="{{'BoxController@summary', $box->id }}">
                                            View
                                          </a>
                                        </p>
                                      </div>






                          </div>
                        </div>
                      </div>

                    </div>

                @endforeach

            @endif



        

      </div>

      <hr>




@endsection