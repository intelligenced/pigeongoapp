@extends('layouts.frontpage')
@section('content')

    <br>
    <div class="is-size-3 ">
        My Claimed Packages
    </div>

      <div class="columns">

        @if($claimed_boxes!=null)

        @foreach($claimed_boxes as $claimed_box)
        <div class="column is-one-third">

            <div class="card">

                    <div class="card-content">
                      <div class="media">
                        <div class="media-left">

                        </div>
                        <div class="media-content">
                          <p class="title is-4"> Box : {{$claimed_box->box()->first()->name}} | From : {{$claimed_box->box()->first()->pickup_address or '?'}} >  To :{{$claimed_box->box()->first()->drop_address or '?'}}</p>
                          <p class="subtitle is-6">@ {{Auth::user()->name}}</p>
                        </div>
                      </div>
                  
                      <div class="content">



                            <br><b>Distance from your location</b> <br>12km<br> 
                            <br><b>Estimated Price</b> <br>20.00 MVR<br> <br>

                            



               






                      </div>
                    </div>
                  </div>

                </div>
              </div>
            @endforeach

        @endif
              </div>


    

        
  </div>

  <div class="is-size-3 ">
      My Deliveries
   </div>


<div class="columns">
            
            @if($boxes!= null)

                @foreach($boxes as $box)
                <div class="column is-one-third">

                <div class="card">

                        <div class="card-content">
                          <div class="media">
                            <div class="media-left">

                            </div>
                            <div class="media-content">
                              <p class="title is-4"> Box : {{$box->name}} | From : {{$box->pickup_address or '?'}} >  To :{{$box->drop_address or '?'}}</p>
                              <p class="subtitle is-6">@ {{Auth::user()->name}}</p>
                            </div>
                          </div>
                      
                          <div class="content">

                                @if($box->box_items()->get()->count() > 0)
                                <br><b>Items</b><br>
                                    @foreach($box->box_items()->get() as $box_item)

                                {{$box_item->name}} ,

                                    @endforeach

                                @endif

                                <div class="subtitle is-6">
                                <br><b>Distance from your location</b><br> 12 km <br> 
                                <br><b>Estimated Price</b><br> 250 MVR <br>
                                </div>
 


                                @if($box->is_claimed_by== null)
                                <div class="has-text-primary">Waiting for Client's Response </div>
    
                              @else
    
                              @if($box->is_delivered == 0)
    
    
                                     <div class="field is-grouped is-grouped-right">
                                              <p class="control">
                                                <a class="button is-outlined is-primary" href="{{ action('BoxController@delivered', $box->id) }}">
                                                    Delivered
                                                </a>
                                              </p>
                                            </div>
    
                              @endif
    
                              @if($box->is_delivered == 1 && $box->is_completed == 0)
    
                              <p>Waiting for Delivery Confirmation </p>
    
                              @endif
    
    
                              @if($box->is_delivered == 1 && $box->is_completed == 0)
                              <p>Box Delivery Completed Successfully </p>
    
                              @endif
    
    
                              @endif


                                

                                            @if($box->is_claimed_by == null)

                                <div class="field is-grouped is-grouped-right">
                                        <p class="control">
                                                <a class="button  is-primary is-outlined" href="{{action('BoxController@claim',$box->id)}}">
                                                        Claim
                                                      </a>
                                        </p>
                                        <p class="control">
                                          <a class="button is-light" href="{{action('BoxController@summary', $box->id)}}">
                                            View
                                          </a>
                                        </p>
                                      </div>

                                      @endif






                          </div>
                        </div>
                      </div>

                    </div>

                @endforeach

            @endif



        

      </div>

@endsection