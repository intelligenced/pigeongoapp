@extends('layouts.frontpage')
@section('content')
 
      
        
        
<div class="is-size-4 ">
    Pick-Up Location
</div>
<br>


{!! Form::open(['action' => 'BoxController@storePickup', 'method' => 'post'])!!}

<input type="hidden" name="box_id" value="{{$box->id}}">
   
    <div id="mapid" style="height:400px;">
    </div>
    {{ Form::hidden('pickup_lat', $box->pickup_lat, array('id' => 'pickUpLocationLat')) }}
    {{ Form::hidden('pickup_long', $box->pickup_long, array('id' => 'pickUpLocationLng')) }}
    <div class="form-group row">
                            <label for="email"  class="col-sm-4 col-form-label text-md-right">Address: </label>

                            <div class="col-md-6">
        <input type="text" name="pickup_address" value="{{$box->pickup_address or ''}}" class="input" required autofocus>
        <div class="field is-grouped is-grouped-right">
    <p class="control">
    <br/>
      <input class="button is-primary" type="submit" value="Update"/>

    </p>
    <p class="control">
    <br/>
      <a class="button is-light">
        Cancel
      </a>
    </p>
  </div>
</div>



</div>

  {!! Form::close() !!}
  

  <a class="button is-primary" href="{{ action('BoxController@step4', $box->id)}}">
      Next
    </a>


<div class="columns">
<div class="column is-one-fifth">



</div>
   






</div>



<script>
$( document ).ready(function() {

var mymap = L.map('mapid').setView(
  [4.175415, 73.510201], 18);
  mapLink = 
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; ' + mapLink + ' Contributors',
            maxZoom: 18,
            }).addTo(mymap);

var popup = L.popup();

var latfield = $('#pickUpLocationLat').val();
var lngfield = $('#pickUpLocationLng').val();


var refLglt = null;
if(latfield != '' & lngfield !=''){



refLglt = L.latLng(latfield, lngfield);
popup
        .setLatLng(refLglt)
        .setContent("Pick-up Area")
        .openOn(mymap);

}else{

}
function onMapClick(e) {

$('#pickUpLocationLat').val(e.latlng.lat);
$('#pickUpLocationLng').val(e.latlng.lng);

    popup
        .setLatLng(e.latlng)
        .setContent("Pick-up Area")
        .openOn(mymap);
}

mymap.on('click', onMapClick);

navigator.geolocation.getCurrentPosition(function(location) {
  var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
  var marker = L.marker(latlng).addTo(mymap);
});


});


</script>

  @endsection