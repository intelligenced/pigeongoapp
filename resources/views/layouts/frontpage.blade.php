<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pigeon Go</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css"
   integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
   crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js"
   integrity="sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q=="
   crossorigin=""></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  </head>
  <body>


        <nav class="navbar is-primary has-shadow is-spaced">
        <div class="navbar-brand">
      
      <!-- Testing comments, this will work hrefo. 
      
                 <a class="navbar-item" href="/">
                       <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
      
                 </a>
      
      
      -->
      
        <a class="navbar-item" href="#">
        <img src="{{ URL::to('/') }}/img/PegionGo_gray.png" style="padding-right:10px;">
                     <h5 class="title" style="color:white;"> <b> Pigeon<span style="color:pink">Go</span>  </b></h5>
         </a>
      
          <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      
        <div id="navbarExampleTransparentExample" class="navbar-menu">
          <div class="navbar-end">
             {{-- <a class="navbar-item" href="/">Home</a>
             <a class="navbar-item" href="/about">About</a> --}}

             @guest
             @else


             <a class="navbar-item" href="{{action('BoxController@browse')}}">
                Browse
              </a>

                           
             <a class="navbar-item" href="{{action('BoxController@mydeliveries')}}">
                My Deliveries
              </a>
             
             <a class="navbar-item" href="{{action('BoxController@step1')}}">
              Add a Box
            </a>

                         
            <a class="navbar-item" href="{{action('BoxController@myboxes')}}">
                My Boxes
              </a>




             <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link" href="/documentation/overview/start/">
                  {{ Auth::user()->name }} 
                </a>

                <div class="navbar-dropdown is-boxed">
                  <a class="navbar-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

            
                </div>
              </div>
              @endif





          </div>
      
      
        </div>
      </nav>



        
        <div class="tabs is-toggle is-fullwidth is-fixed-top">
            <ul>
              <li class="">
                <a style="border:0px" href="{{action('BoxController@myboxes')}}">
                  <span class="icon"><i class="fas fa-box" aria-hidden="true" ></i></span>
                  <span>Add Package</span>
                </a>
              </li>
              <li >
                <a style="border:0px" href="{{action('BoxController@browse')}}">
                  <span class="icon"><i class="fas fa-truck" aria-hidden="true" ></i></span>
                  <span>Deliver Package</span>
                </a>
              </li>
                </ul>
          </div>


      <section class="section ">
            
            <main >
     

              @yield('content')

            </main>
        
        </section>
      
      
      


      <footer class="footer">
            <div class="container">
              <div class="content has-text-centered">
                <p>
                  <strong>PigeonGo</strong> developed by <a href="https://www.itsnicethat.com/system/files/052018/5aec377f7fa44c1b3a000e28/images_slice_large/AndrewGarn-TheNewYorkPigeon-Photography-itsnicethat-06.jpg?1525431401">
                    PigeonTeam</a>.
                <a>2018</a>.
                </p>
              </div>
            </div>
          </footer>

          


  </body>
</html>