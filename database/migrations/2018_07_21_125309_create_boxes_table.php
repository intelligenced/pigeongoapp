<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('due')->nullable();
            $table->integer('box_size_cid')->nullable();
            $table->string('pickup_lat')->nullable();
            $table->string('pickup_long')->nullable();
            $table->string('pickup_address')->nullable();
            $table->string('drop_lat')->nullable();
            $table->string('drop_long')->nullable();
            $table->string('drop_address')->nullable();
            $table->integer('is_claimed')->default(0);
            $table->integer('is_claimed_by')->nullable(); //approved
            $table->integer('is_delivered')->default(0);
            $table->integer('is_completed')->default(0);

            $table->integer('deliverer_rating')->default(0);
            $table->integer('deliveree_rating')->default(0);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
