<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action('BoxController@step1');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('box','BoxController');
Route::get('/step1','BoxController@step1');
Route::post('/step1','BoxController@storestep1');
Route::get('/step2/{box_id}','BoxController@step2');
Route::post('boxitem','BoxController@storeItem');
Route::get('/step3/{box_id}','BoxController@step3');
Route::post('/storePickup','BoxController@storePickup');
Route::get('/step4/{box_id}','BoxController@step4');
Route::post('/storeDropoff','BoxController@storeDropoff');
Route::get('/step5/{box_id}','BoxController@step5');
Route::get('/my','BoxController@myboxes');
Route::get('/browse','BoxController@browse');
Route::get('/claim/{box_id}','BoxController@claim');
Route::get('/myDeliveries','BoxController@mydeliveries');
Route::get('/Map','BoxController@browseMap');
Route::get('/summary/{box_id}','BoxController@summary');
Route::get('/claim/{box_id}/{user_id}','BoxController@approve');
Route::get('/delivered/{box_id}','BoxController@delivered');
Route::get('/completed/{box_id}','BoxController@completed');
